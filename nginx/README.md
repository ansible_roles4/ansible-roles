# nginx

Данная роль используется для развёртывания веб-сервера `nginx`. Далее будет предоставлены возможности для расширения или изменения текущей конфигурации роли.

---

### 1. Настройка переменных

Перед использованием роли убедитесь, что все переменные в файле `nginx/defaults/main.yml` настроены в соответствии с вашими требованиями.

##### Переменные:

| Переменная                        | Значение по умолчанию                        | Описание                                                |
| --------------------------------- | -------------------------------------------- | ------------------------------------------------------- |
| `domain_name`                     | Отсутствует                                  | Доменное имя, используемое для настройки и сертификатов |
| `sudo`                            | "yes"                                        | Использование повышенных привилегий                     |
| `ssh_key`                         | "{{ public_key }}"                           | Открытый ключ SSH для аутентификации                    |
| `proj_name`                       | Отсутствует                                  | Название проекта, используемое в настройках nginx       |
| `nginx.cache_valid_time`          | 3600                                         | Время кеширования для apt в секундах                    |
| `nginx.packages`                  | ['nginx', 'certbot']                         | Пакеты, которые нужно установить (nginx, certbot)       |
| `nginx.config.default_sites_path` | "/etc/nginx/sites-enabled/default"           | Путь к настройкам по умолчанию в nginx                  |
| `nginx.config.project_sites_path` | "/etc/nginx/sites-available/{{ proj_name }}" | Путь к файлам сайта в настройках nginx                  |
| `nginx.config.config_path`        | "/etc/nginx/nginx.conf"                      | Путь к основному конфигурационному файлу nginx          |
| `nginx.config.symlink.src`        | "/etc/nginx/sites-available/{{ proj_name }}" | Исходный путь для символической ссылки                  |
| `nginx.config.symlink.dest`       | "/etc/nginx/sites-enabled/{{ proj_name }}"   | Назначение для символической ссылки                     |
| `letsencrypt.cert_path`           | "/etc/letsencrypt/live/{{ domain_name }}"    | Путь к сертификатам Let's Encrypt                       |
| `letsencrypt.nginx_config_file`   | "nginx.conf"                                 | Файл конфигурации nginx для Let's Encrypt               |
| `letsencrypt.site_file`           | "index.html"                                 | Файл сайта, используемый в конфигурации Let's Encrypt   |
| `letsencrypt.webroot_path`        | "/var/www/{{ proj_name }}"                   | Корневой путь веб-сайта                                 |
| `letsencrypt.email`               | "MAIL@gmail.com"                             | Электронная почта для уведомлений от Let's Encrypt      |
| `letsencrypt.web_owner`           | "www-data"                                   | Владелец веб-файлов                                     |
| `letsencrypt.web_group`           | "www-data"                                   | Группа веб-файлов                                       |
| `letsencrypt.https_config.dest`   | "/etc/nginx/sites-available/{{ proj_name }}" | Назначение HTTPS конфигурации в nginx                   |

---

### 2. Использование шаблонов

В каталоге `nginx/templates` разместите шаблоны Jinja2 для конфигураций HTTP и HTTPS:

- `nginx_http.j2`: базовая HTTP конфигурация, для успешной генерации SSL ключей.
- `nginx_https.j2`: HTTPS конфигурация, включая пути к SSL сертификатам и настройки безопасности.

---

### 3. Статические файлы

По умолчанию за стартовую страницу веб-сервера `nginx` будет отвечать `index.html` размещенный в директории `nginx/files`, по желанию можно поменять этот файл, но стоит использовать имя `index.html` или изменить переменную `defaults/main.yml`.

---

### 4. Задачи Ansible

Роль содержит несколько основных файлов задач в каталоге `nginx/tasks`:

- `main.yml` - содержит в себе другие задачи и их порядок выполнения:
  - `install.yml`: обновление кеша APT и установка необходимых пакетов.
  - `configure.yml`: удаление стандартных файлов nginx, копирование конфигурационных файлов, создание символических ссылок и подготовка каталога веб-сайта.
  - `ssl.yml`: проверка наличия SSL сертификата и его создание при необходимости.
  - `cron_job.yml`: настройка cron задачи для автоматического обновления SSL сертификатов.

---

### 5. Обработчики

Используйте обработчики из файла `nginx/handlers/main.yml` для перезапуска и перезагрузки nginx, а также для обновления сертификатов с помощью Certbot при необходимости.

- **Использование**:
  - `notify: restart` - полный перезапуск `nginx`.
  - `notify: reload` - перечитывание конфиг-файлов `nginx`.

---

### 6. Запуск роли

Для запуска роли, убедитесь, что ваш playbook включает необходимые задачи и обработчики. Пример playbook может выглядеть так:

```yaml
- hosts: all
  become: yes
  roles:
    - nginx
```
